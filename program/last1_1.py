#!/usr/bin/env python
# encoding: utf-8
from __future__ import print_function

try:
    import matplotlib
    matplotlib.use('Agg')
except ImportError:
    pass

import argparse
import chainer
from chainer.datasets import tuple_dataset
import chainer.functions as F
import chainer.links as L
import pandas as pd
import numpy as np
from chainer import training
from chainer.training import extensions
from chainer.training import extension
from chainer.dataset import convert
from chainer.dataset import iterator as iterator_module
from chainer import link
from chainer import reporter as reporter_module
from chainer import variable
import six
import copy
from chainer import serializers
class NNUpdater(training.StandardUpdater):
    def __init__(self,train_iter,optimizer,device):
        super(NNUpdater,self).__init__(
            train_iter,optimizer,device=device)
        self.count = 0

    #the core part of the update rourine can be customized by overriding
    def update_core(self):
        loss = 0
        #When we pass oen iterator and optimizer to StandaedUpdater.__init__
        train_iter = self.get_iterator('main')
        optimizer = self.get_optimizer('main')
        batch = train_iter.__next__()
        optimizer.target.predictor.reset_state()
        x,t = self.converter(batch[:-1],self.device)
        for i in range(x.shape[0]):
            optimizer.target.predictor(chainer.Variable(x[i,:].reshape((1, 153))))

        x,t = self.converter(batch[-1:],self.device) 
        for m in range(1):
            loss += optimizer.target(chainer.Variable(x[m,:].reshape((1, 153))),chainer.Variable(t[m].reshape((1,))))
        
        optimizer.target.cleargrads()#Clear the parameter gradients
        loss.backward()#Backprop
        optimizer.update()#Update the parameters

class Original_Evaluator(extension.Extension):
    trigger = 1, 'epoch'
    default_name = 'validation'
    priority = extension.PRIORITY_WRITER

    def  __init__(self, iterator, target, n_epoch, converter=convert.concat_examples,
                 device=None, eval_hook=None, eval_func=None):
        if isinstance(iterator, iterator_module.Iterator):
            iterator = {'main': iterator}
        self._iterators = iterator
        self.epoch = 1
        self.count = 1
        self.co = 1
        self.save_file = "result_csv/001.csv"
        self.n_epoch = n_epoch
        if isinstance(target, link.Link):
            target = {'main': target}
        self._targets = target
        self.converter = converter
        self.device = device
        self.eval_hook = eval_hook
        self.eval_func = eval_func

    def  get_iterator(self, name):
        """Returns the iterator of the given name."""
        return self._iterators[name]


    def  get_all_iterators(self):
        """Returns a dictionary of all iterators."""
        return dict(self._iterators)


    def  get_target(self, name):
        """Returns the target link of the given name."""
        return self._targets[name]


    def  get_all_targets(self):
        """Returns a dictionary of all target links."""
        return dict(self._targets)

    def __call__(self, trainer=None):
        # set up a reporter
        reporter = reporter_module.Reporter()
        if hasattr(self, 'name'):
            prefix = self.name + '/'
        else:
            prefix = ''
        for name, target in six.iteritems(self._targets):
            reporter.add_observer(prefix + name, target)
            reporter.add_observers(prefix + name,
                                   target.namedlinks(skipself=True))
        with reporter:
            result = self.evaluate()

        reporter_module.report(result)
        return result

    def evaluate(self):
        iterator = self._iterators['main']
        target = self._targets['main']
        target.predictor.Train = False
        eval_func = self.eval_func or target
        if self.eval_hook:
            self.eval_hook(self)
        it = copy.copy(iterator)
        summary = reporter_module.DictSummary()
        for batch in it:
            eval_func.predictor.reset_state()
            observation = {}
            with reporter_module.report_scope(observation):
                in_arrays = self.converter(batch, self.device)
                in_vars = tuple(variable.Variable(x) for x in in_arrays)
                #eval_func(*in_vars)
                x,t = self.converter(batch[:-1],self.device)
                for i in range(x.shape[0]):
                    eval_func.predictor(chainer.Variable(x[i,:].reshape((1, 153))))

                x,t = self.converter(batch[-1:],self.device) 
                for m in range(1):
                    eval_func(chainer.Variable(x[m,:].reshape((1, 153))),chainer.Variable(t[m].reshape((1,))))

                if self.epoch == 1 and self.co == 1:
                    title(self.save_file)
                    self.co = 0
                if self.epoch == self.n_epoch: 
                #if self.epoch % 100 == 0:
                    log_prediction(
                            self.save_file,
                            self.epoch,
                            eval_func.y.data[-1],
                            in_vars[1].data[-1],
                            F.softmax(eval_func.y),
                            self.count
                            )
            summary.add(observation)
        """
        if self.epoch % 10 == 0:
            enter(self.save_file)
        """
        self.epoch += 1
        return summary.compute_mean()

def title(save_file):
    with open(save_file,mode="a") as f :
        f.write(u"{},{},{},{},{}".format("epoch","true_label","softmax_0","softmax_1","pred_label"))
        f.write(u"\n")

def log_prediction(save_file,epoch,_y,_t,y,count):
    with open(save_file,mode="a") as f :
        f.write(u"{},{},{},{},{}".format(epoch,_t,y[-1][0].data,y[-1][1].data,_y.argmax()))
        """
        for i in range(len(y)):
            f.write(u",{}".format(y[i][0].data))
       """
        f.write(u"\n")

def enter(save_file):
    with open(save_file,mode="a") as f :
        f.write("\n")

# Network definition
class MLP(chainer.Chain):
    def __init__(self, n_units, n_out):
        super(MLP, self).__init__()
        with self.init_scope():
            # the size of the inputs to each layer will be inferred
            self.l1 = L.Linear(153, n_units)  # n_in -> n_units
            self.l2 = L.LSTM(n_units, n_units)  # n_units -> n_units
            self.l3 = L.Linear(n_units, 2)  # n_units -> n_out

    def reset_state(self):
        self.l2.reset_state()

    def __call__(self, x):
        #import pdb;pdb.set_trace()
        h1 = F.dropout(F.tanh(self.l1(x)),0.5)
        h2 = F.relu(self.l2(h1))
        y = self.l3(h2)
        return y

    def reset_state(self):
        self.l2.reset_state()

    def __call__(self, x):
        #import pdb;pdb.set_trace()
        h1 = F.tanh(self.l1(x))
        h2 = F.relu(self.l2(h1))
        y = self.l3(h2)
        return y

def main():
    global args
    global count
    parser = argparse.ArgumentParser(description='Chainer example: MNIST')
    parser.add_argument('--batchsize', '-b', type=int, default=5,
                        help='Number of images in each mini-batch')

    parser.add_argument('--epoch', '-e', type=int, default=20,
                        help='Number of sweeps over the dataset to train')

    parser.add_argument('--frequency', '-f', type=int, default=-1,# model保存
                        help='Frequency of taking a snapshot')

    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')

    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')

    parser.add_argument('--resume', '-r', default='',
                        help='Resume the training from snapshot')

    parser.add_argument('--unit', '-u', type=int, default=1024,
                        help='Number of units')

    args = parser.parse_args()

    print('GPU: {}'.format(args.gpu))
    print('# unit: {}'.format(args.unit))
    print('# Minibatch-size: {}'.format(args.batchsize))
    print('# epoch: {}'.format(args.epoch))
    print('')
    # Set up a neural network to train
    # Classifier reports softmax cross entropy loss and accuracy at every
    # iteration, which will be used by the PrintReport extension below.
    model = L.Classifier(MLP(args.unit, 2))

    if args.gpu >= 0:
        # Make a specified GPU current
        chainer.cuda.get_device_from_id(args.gpu).use()
        model.to_gpu()  # Copy the model to the GPU

    # Setup an optimizer
    optimizer = chainer.optimizers.Adam()
    optimizer.setup(model)
    optimizer.use_cleargrads()
    #optimizer.add_hook(chainer.optimizer.GradientClipping(5.0))

    # Load the MNIST dataset
    #train, test = chainer.datasets.get_mnist()
    csvdir = "/home/suoh/csvread/think.csv"
    df = pd.read_csv(csvdir)
    #X = df.ix[:,1:df.shape[1]-1] -> IDがついたら使う
    X = df.ix[:,5:158]
    y = df["T"]
    X = np.array(X).astype(np.float32)
    y = np.array(y).astype(np.int32)


    split = 182580

    x_train = X[:split]
    y_train = y[:split]

    x_test = X[split:]
    y_test = y[split:]

    
    #Y_train = np.concatenate([y_train1,y_train2,y_train3,y_train4,y_train5],axis=1)

    #print(len(X_train))

    #print(len(Y_train))


    
    #out_file = csvdir.replace("lstm_test/NORM_MEAN_0.5_","")
    #out_file = out_file.replace(".csv","")

    train = tuple_dataset.TupleDataset(x_train, y_train)
    test = tuple_dataset.TupleDataset(x_test, y_test)

    train_iter = chainer.iterators.SerialIterator(train, args.batchsize,shuffle=False)
    test_iter = chainer.iterators.SerialIterator(test, args.batchsize,repeat=False, shuffle=False)

    # Set up a trainer
    updater = NNUpdater(train_iter, optimizer, device=args.gpu)
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), out=args.out)
    eval_model = model.copy()
    eval_rnn = eval_model.predictor
    # Evaluate the model with the test dataset for each epoch
    #trainer.extend(extensions.Evaluator(test_iter, model, device=args.gpu,eval_hook=lambda _: [eval_rnn.reset_state(),model.predictor.reset_state()]))
    trainer.extend(Original_Evaluator(
        test_iter,
        model,
        args.epoch,
        device=args.gpu,eval_hook=lambda _: eval_rnn.reset_state()
        ))

    #trainer.extend(Original_Evaluator(test_iter,model,device=args.gpu))
    # Dump a computational graph from 'loss' variable at the first iteration
    # The "main" refers to the target link of the "main" optimizer.
    trainer.extend(extensions.dump_graph('main/loss'))

    # Take a snapshot for each specified epoch
    frequency = args.epoch if args.frequency == -1 else max(1, args.frequency)
    trainer.extend(extensions.snapshot(), trigger=(frequency, 'epoch'))

    # Write a log of evaluation statistics for each epoch
    trainer.extend(extensions.LogReport())

    # Save two plot images to the result dir
    if extensions.PlotReport.available():
        trainer.extend(
            extensions.PlotReport(['main/loss', 'validation/main/loss'],
                                  'epoch', file_name='loss_001.png'))
        
        trainer.extend(
            extensions.PlotReport(
                ['main/accuracy', 'validation/main/accuracy'],
                'epoch', file_name='accuracy_001.png'))
        
    # Print selected entries of the log to stdout
    # Here "main" refers to the target link of the "main" optimizer again, and
    # "validation" refers to the default name of the Evaluator extension.
    # Entries other than 'epoch' are reported by the Classifier link, called by
    # either the updater or the evaluator.
    
    trainer.extend(extensions.PrintReport(
        ['epoch', 'main/loss', 'validation/main/loss',
         'main/accuracy', 'validation/main/accuracy', 'elapsed_time']))
    
    # Print a progress bar to stdout
    trainer.extend(extensions.ProgressBar(update_interval=1))

    if args.resume:
        # Resume from a snapshot
        chainer.serializers.load_npz(args.resume, trainer)

    # Run the training
    trainer.run()


if __name__ == '__main__':
    main()
