# -*- coding: utf-8 -*-
#ELANlabel→TIMESTAMPと同期プログラム
import pandas as pd
import sys

def thinking():
	argv = sys.argv
	argc = len(argv)
	if (argc != 2):
		#引数がちゃんとあるかチェック
		#正しくなければメッセージを出力して終了
		print 'Usage: python %s arg1 arg2' %argv[0]
		quit()

	name = argv[1]
	#コマンドライン上からファイル指定
	C = "/home/suoh/Desktop/1107/label/%s/%s.csv" %(name,name)
	T = "/home/suoh/Desktop/1107/label/%s/%st_yo.txt"%(name,name)
	O = "/home/suoh/Desktop/1107/label/%s/thin%s.csv"%(name,name)
	tsv = pd.read_csv( T, delimiter='\t' )
	csv = pd.read_csv(C)

	label_list = []
	label_result = []
	timestamp_list = []
	#label_list=[開始時刻,終了時刻]
	#timestamp_list=csvのTIMSTAMP

	for i in range(len(tsv.index)):
		label_list.append([tsv.iloc[i,0],tsv.iloc[i,1]])
        for i in range(len(csv.index)):
			timestamp_list.append([csv.ix[i,1]])

	pointer = 0
	label_frag = False 
	for i in range(len(csv.index)):
		if pointer == len(tsv):
			label_result.append(0)
		else :
			if timestamp_list[i][0] * 1000 < label_list[pointer][0]:
				label_frag = False
			if timestamp_list[i][0] * 1000 > label_list[pointer][0]:
				label_frag = True
			if timestamp_list[i][0] * 1000 > label_list[pointer][1]:
				pointer += 1
				label_frag = False
			if label_frag:
				label_result.append(1)
			else:
				label_result.append(0)

	think_label = pd.Series(label_result)
	_output = pd.concat([csv,think_label],axis=1)
	output =_output.rename(columns={0:'T'})
	output.to_csv(O)
	return name

def listening():
	C = "/home/suoh/Desktop/1107/label/%s/%s.csv"%(name,name)
	T = "/home/suoh/Desktop/1107/label/%s/%sl_yo.txt"%(name,name)
	O = "/home/suoh/Desktop/1107/label/%s/liste%s.csv"%(name,name)
	tsv = pd.read_csv( T, delimiter='\t' )
	csv = pd.read_csv(C)

	label_list = []
	label_result = []
	timestamp_list = []
	for i in range(len(tsv.index)):
		label_list.append([tsv.iloc[i,0],tsv.iloc[i,1]])
        for i in range(len(csv.index)):
			timestamp_list.append([csv.ix[i,1]])

	pointer = 0

	label_frag = False 
	for i in range(len(csv.index)):
		if pointer == len(tsv):
			label_result.append(0)
		else :
			if timestamp_list[i][0] * 1000 < label_list[pointer][0]:
				label_frag = False
			if timestamp_list[i][0] * 1000 > label_list[pointer][0]:
				label_frag = True
			if timestamp_list[i][0] * 1000 > label_list[pointer][1]:
				pointer += 1
				label_frag = False

			if label_frag:
				label_result.append(1)
			else:
				label_result.append(0)

	think_label = pd.Series(label_result)
	_output = pd.concat([csv,think_label],axis=1)
	output =_output.rename(columns={0:'L'})
	output.to_csv(O)

if __name__ == '__main__':
    thinking()
    name = thinking()
    listening()
